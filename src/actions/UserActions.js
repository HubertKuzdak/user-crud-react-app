import dispatcher from "../dispatcher";

export function createUser(name) {
    dispatcher.dispatch({
        type : "CREATE_USER",
        name,
    });
}

export function deleteUser(id) {
    dispatcher.dispatch({
        type : "DELETE_USER",
        id,
    });
}

export function updateUser(id,name) {
    dispatcher.dispatch({
        type : "UPDATE_USER",
        id,
        name,
    });
}