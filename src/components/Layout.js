import React from 'react';
import Users from './Users.js';
import UserForm from './UserForm.js';

export default class Layout extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <Users />
                    </div>
                    <div className="col">
                        <UserForm />
                    </div>
                </div>
            </div>
        );
    }
}