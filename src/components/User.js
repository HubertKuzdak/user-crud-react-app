import React from 'react';
import * as UserActions from '../actions/UserActions';

export default class User extends React.Component {
    constructor(){
        super();
        this.state = {
            updateUser : '',
            username : ''
        }
        
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.updateUsername = this.updateUsername.bind(this);
    }
    

    handleClick(e) {
        UserActions.deleteUser(e.target.id);
    }

    handleChange(e){
        if (e.key === 'Enter' && e.target.value.length > 0) {
            UserActions.updateUser(this.props.id,this.state.username);
            this.setState({updateUser : false});
        } else {
            const value = e.target.value;
            this.setState({ username : value });
        }
    }

    updateUsername(){
        this.setState({ username : this.props.title });
        this.setState({ updateUser : true });
    }

    render() {

        const adviceStyle = {
            color: '#ccc',
        };
        const inputStyle = {
            width: '100%',
        };

        const title = this.props.title;
        const id = this.props.id;

        if (this.state.updateUser) {
            return (
                <div>
                    <input itemType = "text" value = { this.state.username } onChange = { this.handleChange } onKeyPress = {this.handleChange} style={ inputStyle } autoFocus />
                    <p style={ adviceStyle }>Press enter to save user</p>
                </div>
            );
        } else {
            return (
                <li >{title} <button type="button" className="btn btn-link" id = {id} onClick = { this.handleClick } >Delete</button> <button type="button" className="btn btn-link" id = {id} onClick = { this.updateUsername } >Edit</button></li>
            );
        }
    }

}