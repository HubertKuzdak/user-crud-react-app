import React from 'react';
import * as UserActions from '../actions/UserActions';

export default class UserForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username : ''
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        console.log(event.target.value);
        console.log(event);
        
    }

    _handleKeyPress(event) {
        if (event.key === 'Enter' && event.target.value.length > 0) {
            const name = event.target.value;
            UserActions.createUser(name);
            event.target.value = '';
        }
    }

    render() {
        return (
            <div>
                <h1>New user</h1>
                <div className="form-group">
                    <input type="text" name="username" onKeyPress={this._handleKeyPress} className="form-control" placeholder="Press enter to submit"/>
                </div>
            </div>
        );
    }
}