import React from 'react';
import User from './User.js';
import UserStore from '../stores/UserStore';

export default class Users extends React.Component {
    constructor(){
        super();
        this.state = {
            users : UserStore.getAllUsers(),
        }
    };

    componentDidMount() {
        UserStore.on("change", () => {
            this.setState({
                users : UserStore.getAllUsers(),
            });
        });
    }

    render() {
        const UsersLength = this.state.users.length;
        const UserList = this.state.users.map((user) =>{
            return <User key = {user.id} id = {user.id} title = {user.name}></User>;
        });

            if(UsersLength > 0){
                return (
                    <div>
                        <h1>Users <span className="badge">({UsersLength})</span></h1>
                        <ul>{UserList}</ul>
                    </div>
                );
            }else{
                return (
                    <div>
                        <h1>Users <span className="badge">({UsersLength})</span></h1>
                        <p>No users</p>
                    </div>
                );                
            }

    }
}