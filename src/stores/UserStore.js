import { EventEmitter } from 'events';

import dispatcher from "../dispatcher";

class UserStore extends EventEmitter {
    constructor(){
        super();
        this.state = {
            users : [
                {
                  "id": 1,
                  "name": "Leanne Graham",
                  "username": "Bret",
                  "email": "Sincere@april.biz",
                  "address": {
                    "street": "Kulas Light",
                    "suite": "Apt. 556",
                    "city": "Gwenborough",
                    "zipcode": "92998-3874",
                    "geo": {
                      "lat": "-37.3159",
                      "lng": "81.1496"
                    }
                  },
                  "phone": "1-770-736-8031 x56442",
                  "website": "hildegard.org",
                  "company": {
                    "name": "Romaguera-Crona",
                    "catchPhrase": "Multi-layered client-server neural-net",
                    "bs": "harness real-time e-markets"
                  }
                },
                {
                  "id": 2,
                  "name": "Ervin Howell",
                  "username": "Antonette",
                  "email": "Shanna@melissa.tv",
                  "address": {
                    "street": "Victor Plains",
                    "suite": "Suite 879",
                    "city": "Wisokyburgh",
                    "zipcode": "90566-7771",
                    "geo": {
                      "lat": "-43.9509",
                      "lng": "-34.4618"
                    }
                  },
                  "phone": "010-692-6593 x09125",
                  "website": "anastasia.net",
                  "company": {
                    "name": "Deckow-Crist",
                    "catchPhrase": "Proactive didactic contingency",
                    "bs": "synergize scalable supply-chains"
                  }
                },
            ],
        }
    };


    getAllUsers(){
        return this.state.users;
    }

    handleActions(action) {
        switch(action.type) {
            case "CREATE_USER" : {
                this.createUser(action.name);
                break;
            }
            case "DELETE_USER" : {
                this.deleteUser(action.id);
                break;
            }
            case "UPDATE_USER" : {
                const userId = action.id;
                const username = action.name;
                this.updateUsername(userId,username);
                break;
            }
        }
    }

    updateUsername(id,name){
        this.state.users.filter((el,index) => {
            if(el.id === id){
                return el.name = name;
            }
        });   

        this.emit("change");
    }

    createUser(name) {
        const id = Date.now();

        this.state.users.push({
            id,
            name,
        });

        this.emit("change");
    }

    deleteUser(id){
        this.state.users.filter((el,index) => {
            if(el.id == id){
                this.state.users.splice(index,1);
            }
        });       
        
        this.emit("change");
    }
}

const userStore = new UserStore();
dispatcher.register(userStore.handleActions.bind(userStore));

export default userStore;